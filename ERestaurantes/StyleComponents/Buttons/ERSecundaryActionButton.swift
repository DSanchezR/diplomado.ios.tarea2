//
//  ERSecundaryActionButton.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 13/03/21.
//

import UIKit

@IBDesignable class ERSecundaryActionButton: ERUIButton {
    
    override func setNeedsLayout() {
        super.setNeedsLayout()
        
        self.style.shape?.radius = 4
        self.style.border?.width = 2
        self.style.border?.color = Colors.primary
        self.backgroundColor = Colors.primary_20
        self.titleLabel?.font = .boldSystemFont(ofSize: 16)
        self.setTitleColor(.white, for: .normal)
    }
}
