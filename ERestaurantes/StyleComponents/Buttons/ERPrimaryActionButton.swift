//
//  ERPrimaryActionButton.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 13/03/21.
//

import UIKit

@IBDesignable class ERPrimaryActionButton: ERUIButton {
    
    var isDisable: Bool = false {
        didSet { self.changeStateDisable() }
    }
    
    private func changeStateDisable() {
    
        UIView.animate(withDuration: 0.3) {
            
            self.alpha = self.isDisable ? 0.5 : 1
            self.isUserInteractionEnabled = !self.isDisable
        }
    }
    
    override func setNeedsLayout() {
        super.setNeedsLayout()
        
        self.style.shape?.radius = 4
        self.style.border?.width = 2
        self.style.border?.color = Colors.primary
        self.backgroundColor = Colors.primary_50
        self.titleLabel?.font = .boldSystemFont(ofSize: 16)
        self.setTitleColor(.white, for: .normal)
    }
}
