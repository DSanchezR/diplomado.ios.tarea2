//
//  EREmailTextField.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 13/03/21.
//

import UIKit

@IBDesignable class EREmailTextField: ERBaseTextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.style.icons?.left.icon = Images.ic_mail
        self.style.icons?.left.tintColor = .white
        
        self.autocorrectionType = .no
        self.keyboardType = .emailAddress
        self.returnKeyType = .next
    }
}
