//
//  ERBaseTextField.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 13/03/21.
//

import UIKit

@IBDesignable class ERBaseTextField: ERUITextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.style.border?.width = 1
        self.style.border?.color = .white
        self.style.shape?.radius = 4
        self.backgroundColor = .clear
        
        self.font = .boldSystemFont(ofSize: 16)
        self.textColor = .white
    }
}
