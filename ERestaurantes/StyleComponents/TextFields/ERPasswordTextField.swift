//
//  ERPasswordTextField.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 13/03/21.
//

import UIKit

@IBDesignable class ERPasswordTextField: ERBaseTextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.style.icons?.left.icon = Images.lock_fill
        self.style.icons?.left.tintColor = .white
        
        self.changeIconPassword()
        
        self.isSecureTextEntry = true
        self.keyboardType = .alphabet
        self.returnKeyType = .next
    }
    
    func changeIconPassword() {
        
        self.isSecureTextEntry = !self.isSecureTextEntry
        self.style.icons?.right.icon = self.isSecureTextEntry ? Images.eye : Images.eye_slash
        self.style.icons?.right.isActive = true
        self.style.icons?.right.tintColor = .white
    }
}
