//
//  ERStatusBarView.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 3/04/21.
//

import UIKit

@IBDesignable class ERStatusBarView: ERUIView {
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.backgroundColor = Colors.primary
    }
}
