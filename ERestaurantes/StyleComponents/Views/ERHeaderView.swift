//
//  ERHeaderView.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 3/04/21.
//

import UIKit

@IBDesignable class ERHeaderView: ERUIView {
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.backgroundColor = Colors.primary
        
        self.style.shadow?.color = .darkGray
        self.style.shadow?.offset = .zero
        self.style.shadow?.opacity = 0.3
        self.style.shadow?.radius = 3
    }
}



