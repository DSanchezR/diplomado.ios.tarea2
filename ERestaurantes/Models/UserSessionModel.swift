//
//  UserSessionModel.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/03/21.
//

import Foundation

struct UserSessionModel {
    
    func doLogin(user: String, password: String, succes: @escaping Success, errorMessage: @escaping ErrorMessage) {
        
        let url     = WebServices.User.login
        let header  = SessionBE.createBasicAuthorization(user: user, password: password)
        
        WebServiceManager.doRequest(.post, urlString: url, header: header) { (response) in
            
            if let json = response as? [String: Any] {
                SessionBE.createWithJson(json)
                succes()
            }else{
                errorMessage(Messages.Error.Login.invalidCredential)
            }
        }
    }
}
