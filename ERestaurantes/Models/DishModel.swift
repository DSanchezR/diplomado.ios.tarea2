//
//  DishModel.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 28/03/21.
//

import Foundation

struct DishModel {
    
    func getDetail(_ session: SessionBE, idDish: String, success: @escaping Dish, errorMessage: @escaping ErrorMessage) {
        
        let url = WebServices.Dish.getDetail(idDish)
        let header = session.headerSession
        
        WebServiceManager.doRequest(.get, urlString: url, header: header) { (response) in
            
            let objectJSONResponse = response as? [String: Any] ?? [:]
            
            let objDish = DishBE(json: objectJSONResponse)
            
            if objDish.dish_id == "" {
                errorMessage(Messages.Error.Dish.notFound)
            }else {
                success(objDish)
            }
        }
    }
    
    func listAll(_ session: SessionBE, success: @escaping Dishes, errorMessage: @escaping ErrorMessage) {
        
        let url = WebServices.Dish.listAll
        let header = session.headerSession
        
        WebServiceManager.doRequest(.get, urlString: url, header: header) { (response) in
            
            let arrayJSONResponse = response as? [[String: Any]] ?? []
            
            if arrayJSONResponse.count != 0 {
                
                var arrayDishes = [DishBE]()
                
                for json in arrayJSONResponse {
                    arrayDishes.append(DishBE(json: json))
                }
                
                success(arrayDishes)
                
            } else {
                errorMessage(Messages.Error.Dish.listEmpty)
            }
        }
    }
}
