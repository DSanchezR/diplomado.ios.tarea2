//
//  ERUITextField.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 13/03/21.
//

import UIKit

@objc protocol ERUITextFieldDelegate {

    @objc optional func textField(_ textField: ERUITextField, tapInLeftIconButton button: UIButton)
    @objc optional func textField(_ textField: ERUITextField, tapInRightIconButton button: UIButton)
}

@IBDesignable class ERUITextField: ERUIIconTextField {
    
    @IBOutlet weak open var textFieldDelegate: ERUITextFieldDelegate?
    
    public lazy var style: Style = {
        return Style(textField: self)
    }()
}

extension ERUITextField {
    
    struct Style {
        
        private weak var textField: ERUITextField?
        
        var inset: ERInsetStyle? {
            get { return self.textField?.inset }
            set { self.textField?.inset = newValue ?? ERInsetStyle() }
        }
        
        var shadow: ERShadowStyle? {
            get { self.textField?.shadowStyle }
            set { self.textField?.shadowStyle = newValue ?? ERShadowStyle() }
        }
        
        var border: ERBorderStyle? {
            get { self.textField?.border_style }
            set { self.textField?.border_style = newValue ?? ERBorderStyle() }
        }
        
        var shape: ERShapeStyle? {
            get { self.textField?.shapeStyle }
            set { self.textField?.shapeStyle = newValue ?? ERShapeStyle() }
        }
        
        var icons: ERUIIconsTextFieldStyle? {
            get { return self.textField?.icons }
            set { self.textField?.icons = newValue ?? ERUIIconsTextFieldStyle( )}
        }
        
        init(textField: ERUITextField) {
            self.textField = textField
        }
    }
}
