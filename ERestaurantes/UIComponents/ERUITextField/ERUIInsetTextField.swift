//
//  ERUIInsetTextField.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 27/02/21.
//

import UIKit

@IBDesignable open class ERUIInsetTextField: UITextField, ERInset {
    
    internal var inset: ERInsetStyle = ERInsetStyle(top: 0, left: 15, bottom: 0, right: 15)
    
    internal var top: CGFloat {
        get { return self.inset.top }
        set { self.inset.top = newValue }
    }
    
    internal var bottom: CGFloat {
        get { return self.inset.bottom }
        set { self.inset.bottom = newValue }
    }
    
    internal var left: CGFloat {
        get { return self.inset.left }
        set { self.inset.left = newValue }
    }
    
    internal var right: CGFloat {
        get { return self.inset.right }
        set { self.inset.right = newValue }
    }
    
    open override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: self.internalInsets)
    }
    
    open override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: self.internalInsets)
    }
        
    open override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: self.internalInsets)
    }

}
