//
//  ERShadow.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/02/21.
//

import UIKit

struct ERShadowStyle {
    
    var color   : UIColor
    var radius  : CGFloat
    var opacity : Float
    var offset  : CGSize
    
    init(color: UIColor = .clear, radius: CGFloat = 5, opacity: Float = 0.4, offset: CGSize = .zero) {
        
        self.color      = color
        self.radius     = radius
        self.opacity    = opacity
        self.offset     = offset
    }
}

protocol ERShadow {
    
    //ESTA PROPIEDAD LA VAMOS A USAR DESDE EL CODIGO
    var shadowStyle     : ERShadowStyle { get set }
    
    //ESTAS PROPIEDADES SOLO LAS VAMOS USAR DESDE EL STORYBOARD
    var shadow_color     : UIColor  { get set }
    var shadow_radius    : CGFloat  { get set }
    var shadow_opacity   : Float    { get set }
    var shadow_offset    : CGSize   { get set }
}

extension ERShadow where Self: UIView {
    
    func updateShadowAppereance() {
        
        self.layer.shadowColor      = self.shadowStyle.color.cgColor
        self.layer.shadowRadius     = self.shadowStyle.radius
        self.layer.shadowOpacity    = self.shadowStyle.opacity
        self.layer.shadowOffset     = self.shadowStyle.offset
    }
}
