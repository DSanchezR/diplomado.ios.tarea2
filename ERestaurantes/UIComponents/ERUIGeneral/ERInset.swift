//
//  ERUIInset.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 27/02/21.
//

import UIKit

public struct ERInsetStyle {
 
    public var top      : CGFloat = 0
    public var bottom   : CGFloat = 0
    public var left     : CGFloat = 0
    public var right    : CGFloat = 0
    
    public static var safeAreaInsets: UIEdgeInsets? {
        return UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.safeAreaInsets
    }
    
    init(top: CGFloat = 0, left: CGFloat = 0, bottom: CGFloat = 0, right: CGFloat = 0) {
        
        self.top    = top
        self.left   = left
        self.right  = right
        self.bottom = bottom
    }
}

protocol ERInset {
    
    var top     : CGFloat { get set }
    var bottom  : CGFloat { get set }
    var left    : CGFloat { get set }
    var right   : CGFloat { get set }
    
    var inset   : ERInsetStyle { get set }
}

extension ERInset where Self: UITextField {
    
    var internalInsets: UIEdgeInsets {
        
        let paddingLeft = self.leftView?.frame.width ?? self.left
        let paddingRight = self.rightView?.frame.width ?? self.right
        
        return UIEdgeInsets(top: self.top,
                            left: paddingLeft,
                            bottom: self.bottom,
                            right: paddingRight)
    }
}

