//
//  ERUIButton.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/02/21.
//

import UIKit

extension ERUIButton {
    
    struct Style {
        
        private weak var button: ERUIButton?
        
        var shadow: ERShadowStyle? {
            get { self.button?.shadowStyle }
            set { self.button?.shadowStyle = newValue ?? ERShadowStyle() }
        }
        
        var border: ERBorderStyle? {
            get { self.button?.border_style }
            set { self.button?.border_style = newValue ?? ERBorderStyle() }
        }
        
        var shape: ERShapeStyle? {
            get { self.button?.shapeStyle }
            set { self.button?.shapeStyle = newValue ?? ERShapeStyle() }
        }
        
        init(button: ERUIButton) {
            self.button = button
        }
    }
}

@IBDesignable class ERUIButton: ERUIShadowButton {
    
    lazy var style: Style = {
        return Style(button: self)
    }()
}
