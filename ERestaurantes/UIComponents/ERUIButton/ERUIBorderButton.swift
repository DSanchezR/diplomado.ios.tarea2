//
//  ERUIBorderButton.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/02/21.
//

import UIKit

@IBDesignable class ERUIBorderButton: UIButton, ERBorder {
    
    //MARK: - BORDER
    var border_style: ERBorderStyle = ERBorderStyle() {
        didSet { self.updateBorderAppereance() }
    }
    
    @IBInspectable internal var borderColor: UIColor {
        get { self.border_style.color }
        set { self.border_style.color = newValue }
    }
    
    @IBInspectable internal  var borderWidth: CGFloat {
        get { self.border_style.width }
        set { self.border_style.width = newValue }
    }
}
