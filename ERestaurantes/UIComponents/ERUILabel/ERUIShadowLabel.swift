//
//  ERUIShadowLabel.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 27/02/21.
//

import UIKit

@IBDesignable class ERUIShadowLabel: ERUIInsetLabel, ERShadow {
        
    //MARK: - SHADOW
    
    var shadowStyle: ERShadowStyle = ERShadowStyle() {
        didSet { self.updateShadowAppereance() }
    }
    
    @IBInspectable internal var shadow_color: UIColor {
        get { self.shadowStyle.color }
        set { self.shadowStyle.color = newValue }
    }
    
    @IBInspectable internal var shadow_radius: CGFloat {
        get { self.shadowStyle.radius }
        set { self.shadowStyle.radius = newValue }
    }
    
    @IBInspectable internal var shadow_opacity: Float {
        get { self.shadowStyle.opacity }
        set { self.shadowStyle.opacity = newValue }
    }
    
    @IBInspectable internal var shadow_offset: CGSize {
        get { self.shadowStyle.offset }
        set { self.shadowStyle.offset = newValue }
    }
}

