//
//  ERUILabel.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 27/02/21.
//

import Foundation

@IBDesignable class ERUILabel: ERUIShadowLabel {
    
    public lazy var style: Style = {
        return Style(label: self)
    }()
}

extension ERUILabel {
    
    struct Style {
        
        private weak var label: ERUILabel?
        
        init(label: ERUILabel) {
            self.label = label
        }

        public var shadow: ERShadowStyle? {
            get { return self.label?.shadowStyle }
            set { self.label?.shadowStyle = newValue ?? ERShadowStyle() }
        }
        
        public var inset: ERInsetStyle? {
            get { return self.label?.inset }
            set { self.label?.inset = newValue ?? ERInsetStyle() }
        }
    }
}

