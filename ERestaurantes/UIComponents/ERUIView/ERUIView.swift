//
//  ERUIView.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/02/21.
//

import UIKit

extension ERUIView {
    
    struct Style {
        
        private weak var view: ERUIView?
        
        var shadow: ERShadowStyle? {
            get { self.view?.shadowStyle }
            set { self.view?.shadowStyle = newValue ?? ERShadowStyle() }
        }
        
        var border: ERBorderStyle? {
            get { self.view?.border_style }
            set { self.view?.border_style = newValue ?? ERBorderStyle() }
        }
        
        var shape: ERShapeStyle? {
            get { self.view?.shapeStyle }
            set { self.view?.shapeStyle = newValue ?? ERShapeStyle() }
        }
        
        init(view: ERUIView) {
            self.view = view
        }
    }
}

@IBDesignable class ERUIView: ERUIShadowView {
    
    lazy var style: Style = {
        Style(view: self)
    }()
}
