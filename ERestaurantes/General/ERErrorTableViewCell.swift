//
//  ERErrorTableViewCell.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 3/04/21.
//

import UIKit

class ERErrorTableViewCell: UITableViewCell {
    
    @IBOutlet weak private var lblErrorMessage: UILabel!
    
    var errorMessage: String! {
        didSet { self.reloadData() }
    }
    
    private func reloadData() {
        self.lblErrorMessage.text = self.errorMessage
    }
    
    class func createInTableView(_ tableView: UITableView, indexPath: IndexPath, errorMessage: String) -> ERErrorTableViewCell {
        
        let cellIdentifier = "ERErrorTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ERErrorTableViewCell
        cell?.errorMessage = errorMessage
        return cell ?? ERErrorTableViewCell()
    }
}
