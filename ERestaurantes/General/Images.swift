//
//  Images.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 13/03/21.
//

import UIKit

struct Images {
    
    static let ic_mail      = UIImage(systemName: "mail")
    static let lock_fill    = UIImage(systemName: "lock.fill")
    static let eye          = UIImage(systemName: "eye")
    static let eye_slash    = UIImage(systemName: "eye.slash")
}
