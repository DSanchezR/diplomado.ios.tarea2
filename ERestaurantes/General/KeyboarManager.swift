//
//  FormKeyboarManager.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 13/03/21.
//

import UIKit

extension KeyboarManager {
    
    class KeyboardInformation: NSObject {
        
        var frame               : CGRect
        var size                : CGSize
        var animationDuration   : Double
        
        fileprivate init(_ notification: Notification) {
            
            self.animationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0
            self.frame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect ?? .zero
            self.size = self.frame.size
        }
    }
}

@objc protocol KeyboarManagerDelegate {
    
    @objc optional func keyboarManager(_ keyboardManager: KeyboarManager, keyboardWillShowInformation keyboardInformation: KeyboarManager.KeyboardInformation)
    @objc optional func keyboarManager(_ keyboardManager: KeyboarManager, keyboardWillHideInformation keyboardInformation: KeyboarManager.KeyboardInformation)
}

class KeyboarManager: NSObject {
    
    private weak var delegate: KeyboarManagerDelegate?
    
    init(delegate: KeyboarManagerDelegate) {
        self.delegate = delegate
    }
    
    func registerKeyboardNotifications() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifictions() {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        
        let keyboardAttributes = KeyboardInformation(notification)
        self.delegate?.keyboarManager?(self, keyboardWillShowInformation: keyboardAttributes)
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {

        let keyboardAttributes = KeyboardInformation(notification)
        self.delegate?.keyboarManager?(self, keyboardWillHideInformation: keyboardAttributes)
    }
}
