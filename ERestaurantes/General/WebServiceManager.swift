//
//  WebServiceManager.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 27/03/21.
//

import Foundation

extension WebServiceManager {
    typealias CompletionHandler = (_ jsonResponse: Any?) -> Void
}

extension WebServiceManager {
    
    enum Method: String {
        case post   = "POST"
        case get    = "GET"
        case put    = "PUT"
        case delete = "DELETE"
    }
}

class WebServiceManager {
    
    class func doRequest(_ httpMethod: Method, urlString: String, header: [String: Any]? = nil, params: Any? = nil, completion: CompletionHandler? = nil) {
        
        guard let urlService = URL(string: urlString) else {
            print("La url no tiene el formato correcto")
            return
        }
        
        var urlRequest = URLRequest(url: urlService)
        urlRequest.httpMethod = httpMethod.rawValue
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = self.createHeaderRquest(header)
        
        //URL SESSION ES LA SESIÓN DE CONEXIÓN AL SERVIDOR
        let urlSession = URLSession(configuration: sessionConfiguration)
        
        httpMethod == .get ?
                        self.downloadDataTask(urlSession, urlRequest: urlRequest, completion: completion) :
                        self.updloadDataTask(urlSession, urlRequest: urlRequest, params: params, completion: completion)
    }
    
    private class func createHeaderRquest(_ additionalHeaders: [String: Any]?) -> [String: Any] {
        
        var headerRequest = [String: Any]()
        
        headerRequest["Content-Type"] = "application/json"
        
        for element in additionalHeaders ?? [:]  {
            headerRequest[element.key] = element.value
        }
        
        return headerRequest
    }
    
    private class func updloadDataTask(_ urlSession: URLSession, urlRequest: URLRequest, params: Any?, completion: CompletionHandler?) {
        
        var dataParams: Data? = nil
        
        if let params = params {
            dataParams = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        }
        
        let task = urlSession.uploadTask(with: urlRequest, from: dataParams ?? Data()) { (data, urlResponse, error) in
            
            let jsonResponse = self.getJSONResponseFromData(data)
            
            DispatchQueue.main.async {
                completion?(jsonResponse)
            }
        }
        
        task.resume()
    }
    
    
    private class func downloadDataTask(_ urlSession: URLSession, urlRequest: URLRequest, completion: CompletionHandler?) {
        
        let task = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            let jsonResponse = self.getJSONResponseFromData(data)
            
            DispatchQueue.main.async {
                completion?(jsonResponse)
            }
        }
        
        task.resume()
    }
    
    private class func getJSONResponseFromData(_ data: Data?) -> Any? {
        
        guard let data = data else { return nil }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
        print(jsonResponse ?? "Error en la lectura de la trama")
        return jsonResponse
    }
}
