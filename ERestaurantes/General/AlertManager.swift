//
//  AlertManager.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/03/21.
//

import UIKit

extension UIViewController {
    
    typealias AcceptHandler = ()->Void
    
    func showAlert(title: String, message: String, acceptButtonText: String, acceptHandler: AcceptHandler? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let acceptAction = UIAlertAction(title: acceptButtonText, style: .cancel) { (_) in
            acceptHandler?()
        }
        
        alertController.addAction(acceptAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
