//
//  Closures.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/03/21.
//

import Foundation

typealias Success           = () -> Void
typealias ErrorMessage      = (_ errorMessage: String) -> Void
typealias Dishes            = (_ arrayDishes: [DishBE]) -> Void
typealias Dish              = (_ objDish: DishBE) -> Void
