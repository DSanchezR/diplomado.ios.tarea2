//
//  Messages.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/03/21.
//

import Foundation

struct Messages {
    
    struct Alert {
        
        static let acceptButton = "Aceptar"
        static let title        = "Mi Restaurante"
        static let errorTitle   = "Error"
    }
    
    struct Error {
        
        struct Login {
            static let invalidUser          = "Debes ingresar un usuario válido"
            static let invalidPassword      = "Debes ingresar un password válido"
            static let invalidCredential    = "El usuario o la contraseña son incorrectas"
        }
        
        struct Dish {
            static let listEmpty            = "Los platos no se encuentran disponibles temporalmente."
            static let notFound             = "El elemento no se encuentra disponible"
        }
        
        struct Profile {
            
            
        }
    }
}
