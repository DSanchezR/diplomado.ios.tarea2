//
//  WebServices.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 28/03/21.
//

import Foundation

struct WebServices {
    
    static private let baseURL = "https://restaurant-administrador-am.herokuapp.com/"
    
    struct User {
        static let login = baseURL + "auth-client"
    }
    
    struct Dish {
        static let listAll = baseURL + "dishes"
        
        static func getDetail(_ idDish: String) -> String {
            return baseURL + "dishes/\(idDish)/show"
        }
    }
}
