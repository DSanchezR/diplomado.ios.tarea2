//
//  Colors.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 13/03/21.
//

import UIKit

struct Colors {
    
    static let primary      = UIColor(displayP3Red: 193/255.0, green: 11/255.0, blue: 25/255.0, alpha: 1)
    static let primary_50   = UIColor(displayP3Red: 193/255.0, green: 11/255.0, blue: 25/255.0, alpha: 0.5)
    static let primary_30   = UIColor(displayP3Red: 193/255.0, green: 11/255.0, blue: 25/255.0, alpha: 0.3)
    static let primary_20   = UIColor(displayP3Red: 193/255.0, green: 11/255.0, blue: 25/255.0, alpha: 0.2)
}
