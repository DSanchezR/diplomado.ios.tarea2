//
//  DishBE.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 28/03/21.
//

import Foundation

extension DishBE {
 
    struct Category {
        
        let category_id     : String
        let category_name   : String
        
        init(json: [String: Any]?) {
            
            self.category_id    = json?["_id"] as? String ?? ""
            self.category_name  = json?["name"] as? String ?? ""
        }
    }
}

struct DishBE {
    
    let dish_id         : String
    let dish_name       : String
    let dish_avatar     : String
    let dish_price      : Int
    let dish_description: String
    let dish_category   : Category
    
    var dish_priceFormat: String {
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "es_PE")
        format.numberStyle = .currency
        let priceNumber = NSNumber(value: self.dish_price)
        return format.string(from: priceNumber) ?? "--"
    }
    
//    var dish_priceFormat: String {
//        return "S/ \(self.dish_price)"
//    }
    
    init(json: [String: Any]?) {
 
        self.dish_id            = json?["_id"] as? String ?? ""
        self.dish_name          = json?["name"] as? String ?? ""
        self.dish_avatar        = json?["avatar"] as? String ?? "" //TODO: Completar con endPoint para descarga de imagen
        self.dish_price         = json?["price"] as? Int ?? 0
        self.dish_description   = json?["description"] as? String ?? ""
        self.dish_category      = Category(json: json?["category"] as? [String: Any])
    }
}
