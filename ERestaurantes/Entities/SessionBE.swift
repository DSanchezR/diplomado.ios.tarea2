//
//  SessionBE.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/03/21.
//

import Foundation

struct SessionBE {
    
    static private var shared: SessionBE?
    
    let session_clientId: String
    let session_token   : String
    
    var headerSession: [String: Any] {
        return ["token" : self.session_token]
    }
    
    static var instance: SessionBE? {
        return self.shared
    }
    
    private init(json: [String: Any]) {
        
        self.session_clientId   = json["client_id"] as? String ?? ""
        self.session_token      = json["token"] as? String ?? ""
    }
    
    @discardableResult static func createWithJson(_ json: [String: Any]) -> SessionBE {
        let newSession      = SessionBE(json: json)
        SessionBE.shared    = newSession
        return newSession
    }
    
    static func createBasicAuthorization(user: String, password: String) -> [String: Any] {
        
        let stringCredential    = "\(user):\(password)"
        let dataCredential      = stringCredential.data(using: .utf8) ?? Data()
        let base64Credential    = dataCredential.base64EncodedString()
        let authCredential      = "Basic \(base64Credential)"
        
        return ["Authorization" : authCredential]
    }
}
