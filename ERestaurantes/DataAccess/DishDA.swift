//
//  DishDA.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 3/04/21.
//

import Foundation
import CoreData

class DishDA {
    
    func addDish(_ objDish: DishBE, context: NSManagedObjectContext) -> DishDataModel? {
        
        let objDM = NSEntityDescription.insertNewObject(forEntityName: "DishDataModel", into: context) as? DishDataModel
        
        objDM?.dishId       = objDish.dish_id
        objDM?.name         = objDish.dish_name
        objDM?.price        = Int16(objDish.dish_price)
        objDM?.avatar       = objDish.dish_avatar
        objDM?.resume       = objDish.dish_description
        objDM?.categoryId   = objDish.dish_category.category_id
        objDM?.categoryName = objDish.dish_category.category_name
        
        return objDM
    }
    
    func listAll(_ context: NSManagedObjectContext) -> [DishDataModel] {
        
        let fetchRequest: NSFetchRequest<DishDataModel> = DishDataModel.fetchRequest()
        
        let sortPrice = NSSortDescriptor(key: "price", ascending: true)
        let sortName = NSSortDescriptor(key: "name", ascending: true)
        
        fetchRequest.sortDescriptors = [sortPrice, sortName]
        
        let result = try? context.fetch(fetchRequest)
        return result ?? []
    }
}
