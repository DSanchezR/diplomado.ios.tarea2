//
//  DishesPresenter.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 28/03/21.
//

import Foundation

class DishesPresenter {
    
    private let controller: ERProductsViewController
    
    lazy private var model: DishModel = {
       return DishModel()
    }()
    
    init(controller: ERProductsViewController) {
        self.controller = controller
    }
}

extension DishesPresenter {
    
    func listAll() {
        
        guard let session = SessionBE.instance else {
            self.controller.reloadContent(["La sesión es inválida. Vuelva a iniciar sesión"])
            return
        }
        
        self.controller.showLoadingList(true)
        
        self.model.listAll(session) { (arrayDishes) in
            
            self.controller.showLoadingList(false)
            self.controller.reloadContent(arrayDishes)
            
        } errorMessage: { (errorMessage) in
            
            self.controller.showLoadingList(false)
            self.controller.reloadContent([errorMessage])
        }
    }
    
    func openDishDetailToIndexPath(_ indexPath: IndexPath) {
        self.controller.openDishDetailToIndexPath(indexPath)
    }
}

extension DishesPresenter: LifeCyclePresenter {
    
    func viewDidLoad() {
        
        self.controller.addPullToRefreshControl()
        self.listAll()
    }
}
