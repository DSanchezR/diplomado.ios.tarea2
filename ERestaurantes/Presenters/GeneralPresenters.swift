//
//  GeneralPresenters.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/03/21.
//

import UIKit


//MARK: - LifeCyclePresenter
@objc protocol LifeCyclePresenter {
    
    @objc optional func viewDidLoad()
    @objc optional func viewWillAppear()
    @objc optional func viewDidAppear()
    @objc optional func viewWillDisappear()
    @objc optional func viewDidDisappear()
}
