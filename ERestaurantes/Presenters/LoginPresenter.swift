//
//  LoginPresenter.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/03/21.
//

import UIKit

class LoginPresenter {

    private let controller: LoginViewController
    
    lazy private var model: UserSessionModel = {
       return UserSessionModel()
    }()
    
    init(controller: LoginViewController) {
        self.controller = controller
    }
}

//MARK: - Actions
 
extension LoginPresenter {
    
    func doLogin(user: String?, password: String?) {
        
        guard let user = user, user.count != 0 else {
            self.controller.showAlert(title             : Messages.Alert.errorTitle,
                                      message           : Messages.Error.Login.invalidUser,
                                      acceptButtonText  : Messages.Alert.acceptButton)
            return
        }
        
        guard let password = password, password.count != 0 else {
            self.controller.showAlert(title             : Messages.Alert.errorTitle,
                                      message           : Messages.Error.Login.invalidPassword,
                                      acceptButtonText  : Messages.Alert.acceptButton)
            return
        }
        
        self.controller.loading(true)
        
        self.model.doLogin(user: user, password: password) {
            
            self.controller.loading(false)
            self.controller.goToTabBarController()
            
        } errorMessage: { (errorMessage) in
            
            self.controller.loading(false)
            self.controller.showAlert(title             : Messages.Alert.errorTitle,
                                      message           : errorMessage,
                                      acceptButtonText  : Messages.Alert.acceptButton)
        }
    }
    
    func closeKeyboard() {
        self.controller.view.endEditing(true)
    }
    
    private func doLogin() {
        self.doLogin(user: self.controller.txtEmail.text, password: self.controller.txtPassword.text)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
            case self.controller.txtEmail      : self.controller.txtPassword.becomeFirstResponder()
            case self.controller.txtPassword   : self.doLogin()
            default                 : return true
        }
        
        return true
    }
}

//MARK: - LifeCyclePresenter

extension LoginPresenter: LifeCyclePresenter {

    func viewDidLoad() {
        self.controller.setInitViewState()
    }
    
    func viewWillAppear() {
        self.controller.keyboardManager.registerKeyboardNotifications()
    }
    
    func viewDidAppear() {
        self.controller.appearContent()
    }
    
    func viewWillDisappear() {
        self.controller.keyboardManager.unregisterKeyboardNotifictions()
    }
}

//MARK: - KeyboarManagerDelegate

extension LoginPresenter: KeyboarManagerDelegate {
    
    func keyboarManager(_ keyboardManager: KeyboarManager, keyboardWillShowInformation keyboardInformation: KeyboarManager.KeyboardInformation) {
        self.controller.keyboardWillShow(keyboardInformation)
    }
    
    func keyboarManager(_ keyboardManager: KeyboarManager, keyboardWillHideInformation keyboardInformation: KeyboarManager.KeyboardInformation) {
        self.controller.keyboardWillHide(keyboardInformation)
    }
}

