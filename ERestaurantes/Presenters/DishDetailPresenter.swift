//
//  DishDetailPresenter.swift
//  ERestaurantes
//
//  Created by David Gregori Sánchez Rafael on 3/04/21.
//

import Foundation

class DishDetailPresenter {
    
    private let controller: ERDishDetailViewController
    
    lazy private var model: DishModel = {
       return DishModel()
    }()
    
    init(controller: ERDishDetailViewController) {
        self.controller = controller
    }
}

extension DishDetailPresenter {
    
    func cleanUI() {
        self.controller.setInitViewState()
    }
    
    func getDetail() {

        guard let session = SessionBE.instance else {
            self.controller.notifyError(messageError: "La sesión es inválida. Vuelva a iniciar sesión")
            return
        }

        self.model.getDetail(session, idDish: self.controller.idDish) { (objDish) in

            self.controller.loadContent(objDish)

        } errorMessage: { (errorMessage) in

            self.controller.notifyError(messageError: errorMessage)
        }
        
    }
    
}

extension DishDetailPresenter: LifeCyclePresenter {
    
    func viewDidLoad() {
        self.getDetail()
    }
}
