//
//  LoginViewController.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/02/21.
//

import UIKit

class LoginViewController: UIViewController {
 
    @IBOutlet weak var btnLogin                 : ERPrimaryActionButton!
    @IBOutlet weak var viewContentFields        : ERUIView!
    @IBOutlet weak var cnsCenterYContentFields  : NSLayoutConstraint!
    @IBOutlet weak var txtEmail                 : EREmailTextField!
    @IBOutlet weak var txtPassword              : ERPasswordTextField!
    @IBOutlet weak var actvLoading              : UIActivityIndicatorView!
    
    lazy var keyboardManager: KeyboarManager = {
        return KeyboarManager(delegate: self.presenter)
    }()
    
    lazy private var presenter: LoginPresenter = {
        return LoginPresenter(controller: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presenter.viewDidAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter.viewWillDisappear()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}


//MARK: Other Methods

extension LoginViewController {
    
    @IBAction private func tapToCloseKeyboard(_ sender: Any) {
        self.presenter.closeKeyboard()
    }
    
    @IBAction private func tapToBtnLogin(_ sender: Any) {
        
        self.presenter.doLogin(user     : self.txtEmail.text,
                               password : self.txtPassword.text)
    }
    
    func loading(_ isLoading: Bool) {
        
        isLoading ? self.actvLoading.startAnimating() : self.actvLoading.stopAnimating()
        self.view.isUserInteractionEnabled = !isLoading
        self.btnLogin.isDisable = isLoading
    }

    func setInitViewState() {
        
        let delta = self.view.frame.height - self.viewContentFields.center.y
        self.cnsCenterYContentFields.constant = delta
        self.viewContentFields.alpha = 0
        self.view.layoutIfNeeded()
    }
    
    func appearContent() {
        
        UIView.animate(withDuration: 1.5, delay: 1, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.3, options: .curveEaseOut) {
            
            self.cnsCenterYContentFields.constant = 0
            self.viewContentFields.alpha = 1
            self.view.layoutIfNeeded()
            
        } completion: { (_) in
            
        }
    }
    
    func goToTabBarController() {
        self.performSegue(withIdentifier: "TabBarController", sender: nil)
    }
    
    func keyboardWillShow(_ keyboardInformation: KeyboarManager.KeyboardInformation) {
        
        let endYPointContentFields = self.viewContentFields.frame.origin.y + self.viewContentFields.frame.size.height + 20

        if keyboardInformation.frame.origin.y < endYPointContentFields {

            UIView.animate(withDuration: keyboardInformation.animationDuration) {
                self.cnsCenterYContentFields.constant = keyboardInformation.frame.origin.y - endYPointContentFields
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func keyboardWillHide(_ keyboardInformation: KeyboarManager.KeyboardInformation) {
        
        UIView.animate(withDuration: keyboardInformation.animationDuration) {
            self.cnsCenterYContentFields.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return self.presenter.textFieldShouldReturn(textField)
    }
}

extension LoginViewController: ERUITextFieldDelegate {
    
    func textField(_ textField: ERUITextField, tapInRightIconButton button: UIButton) {
        
//        if let textField = textField as? ERPasswordTextField {
//            textField.changeIconPassword()
//        }
    }
}
