//
//  ERProductsViewController.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/03/21.
//

import UIKit

class ERProductsViewController: UIViewController {

    @IBOutlet weak var tblDishes: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(self.pullToRefresh), for: .valueChanged)
        return control
    }()
    
    var arrayContent = [Any]()
    
    private lazy var presenter: DishesPresenter = {
        return DishesPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let controller = segue.destination as? ERDishDetailViewController {
            controller.idDish = sender as? String ?? ""
        }
    }
}

//MARK: - Other Methods
extension ERProductsViewController {
    
    func addPullToRefreshControl() {
        self.tblDishes.addSubview(self.refreshControl)
    }
    
    func reloadContent(_ arrayContent: [Any]) {
        
        self.arrayContent = arrayContent
        let indexSet = IndexSet(integer: 0)
        self.tblDishes.reloadSections(indexSet, with: .fade)
    }
    
    func showLoadingList(_ show: Bool) {
        show ? self.refreshControl.beginRefreshing() : self.refreshControl.endRefreshing()
    }
    
    @objc private func pullToRefresh() {
        self.presenter.listAll()
    }
    
    func openDishDetailToIndexPath(_ indexPath: IndexPath) {
        
        if let objDish = self.arrayContent[indexPath.row] as? DishBE {
            self.performSegue(withIdentifier: "ERDishDetailViewController", sender: objDish.dish_id)
        }
    }
}

//MARK: - UITableViewDataSource
extension ERProductsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayContent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let element = self.arrayContent[indexPath.row]
        
        if let objDish = element as? DishBE {
            return ERDishTableViewCell.createInTableView(tableView, indexPath: indexPath, objDish: objDish)
            
        } else if let errorMessage = element as? String {
            return ERErrorTableViewCell.createInTableView(tableView, indexPath: indexPath, errorMessage: errorMessage)
            
        } else {
            return UITableViewCell()
        }
    }
}

//MARK: - UITableViewDelegate
extension ERProductsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.openDishDetailToIndexPath(indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let element = self.arrayContent[indexPath.row]
        return (element is DishBE) ? UITableView.automaticDimension : tableView.frame.height
    }
}
