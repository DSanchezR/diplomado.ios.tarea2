//
//  ERDishTableViewCell.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 3/04/21.
//

import UIKit

class ERDishTableViewCell: UITableViewCell {
    
    @IBOutlet weak private var imgDish          : UIImageView!
    @IBOutlet weak private var lblPrice         : UILabel!
    @IBOutlet weak private var lblName          : UILabel!
    @IBOutlet weak private var lblCategoryName  : UILabel!
    
    var objDish: DishBE! {
        didSet { self.reloadData() }
    }
    
    private func reloadData() {
        
        self.imgDish.backgroundColor    = .white
        self.imgDish.image              = UIImage(named: self.objDish.dish_avatar)
        self.lblName.text               = self.objDish.dish_name
        self.lblCategoryName.text       = self.objDish.dish_category.category_name
        self.lblPrice.text              = self.objDish.dish_priceFormat
    }
    
    class func createInTableView(_ tableView: UITableView, indexPath: IndexPath, objDish: DishBE) -> ERDishTableViewCell {
        
        let cellIdentifier = "ERDishTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ERDishTableViewCell
        cell?.objDish = objDish
        return cell ?? ERDishTableViewCell()
    }
}
