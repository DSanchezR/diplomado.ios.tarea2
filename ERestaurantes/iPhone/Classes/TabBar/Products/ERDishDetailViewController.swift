//
//  ERDishDetailViewController.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 3/04/21.
//

import UIKit

class ERDishDetailViewController: UIViewController {
    
    @IBOutlet weak var imgDish: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSubTitleCategory: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var scrollViewDishDetail: UIScrollView!
    @IBOutlet weak var actvLoading: UIActivityIndicatorView!
    
    var idDish: String = ""
    var objDish: DishBE?
    
    private lazy var presenter: DishDetailPresenter = {
        return DishDetailPresenter(controller: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.cleanUI()
        self.presenter.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: Other Methods
extension ERDishDetailViewController {
    
    @IBAction func clickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setInitViewState() {
        
        self.imgDish.image = .none
        self.imgDish.backgroundColor = .white
        self.lblName.text = ""
        self.lblCategory.text = ""
        self.lblDescription.text = ""
        self.lblPrice.text = ""
        self.lblSubTitleCategory.text = ""
        self.actvLoading.startAnimating()
        
    }
    
    func loadContent(_ objDish: DishBE) {
        
        self.objDish = objDish
        
        // Cargar los valores del objeto en la UI
        
        self.imgDish.image = UIImage(named: self.objDish?.dish_avatar ?? "avatar")
        self.lblName.text = self.objDish?.dish_name
        self.lblSubTitleCategory.text = "Categoría:"
        self.lblCategory.text = self.objDish?.dish_category.category_name
        self.lblDescription.text = self.objDish?.dish_description
        self.lblPrice.text = self.objDish?.dish_priceFormat
        
        self.actvLoading.stopAnimating()
    }
    
    func notifyError(messageError message: String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)    }
    
}
